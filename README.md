# roblox-rap-checker

A simple roblox RAP checker written in JavaScript

## Example

    const ROBLOX = require("./lib/roblox")
    
    new ROBLOX(1).getRAP()
        .then((data) => {
            console.log(JSON.stringify(data))
        })
        .catch((err) => {
            console.log("Failure grabbing RAP for user.")
            console.error(err.stack)
        })

Output: `{"RAP":43947860,"time":3.524}`