const p = require("phin")

const inventoryRequest = p.defaults({
    method: "GET",
    parse: "json"
})

const assetTypes = [
    "Hat", 
    "Gear", 
    "Face", 
    "HairAccessory", 
    "FaceAccessory", 
    "NeckAccessory", 
    "ShoulderAccessory", 
    "FrontAccessory", 
    "BackAccessory", 
    "WaistAccessory"
]

 module.exports = 
 
 class ROBLOX {
    constructor(id) {
        this.start = new Date()
        this.id = id
        this.RAP = 0
    }

    async getRAPforAssetType(asset, cursor = "") {
        const req = await inventoryRequest(
            `https://inventory.roblox.com/v1/users/${this.id}/assets/collectibles?assetType=${asset}&sortOrder=Asc&limit=100&cursor=${cursor}`
        )

        if(req.body.errors) {
            throw new Error(req.body.errors[0].message)
        }

        const itemData = req.body.data

        for(let item in itemData) {
            const props = itemData[item]
            this.RAP += props.recentAveragePrice
        }

        // Check for a page cursor and re-run the function to get the RAP for the next page
        if(req.body.nextPageCursor) {
            return this.getRAPforAssetType(asset, req.body.nextPageCursor)
        }
    }

    async getRAP() {
        let requests = []

        for(let asset of assetTypes) {
            await requests.push(this.getRAPforAssetType(asset))
        }

        // Wait for all of the requests to succeed before returning the RAP
        await Promise.all(requests)

        return {
            RAP: this.RAP,
            time: (new Date() - this.start) / 1000
        }
    }
}