const ROBLOX = require("./lib/roblox")

new ROBLOX(1).getRAP()
    .then((data) => {
        console.log(JSON.stringify(data))
    })
    .catch((err) => {
        console.log("Failure grabbing RAP for user.")
        console.error(err.stack)
    })